import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        List<List<String>> data_train = readData("number-training.csv");
        List<List<String>> data_test = readData("number-testing.csv");

        List<String> results = bayesian_classification(data_train, data_test);

        System.out.println("Label compare");
        System.out.println("Test\t|\tResult");
        int data_test_label_index = data_test.get(0).size() - 1;
        for (int i = 0; i < data_test.size(); i++) {
            System.out.println(data_test.get(i).get(data_test_label_index) + "\t|\t" + results.get(i));
        }

        double error = results_error(data_test, results, true);
        System.out.println("Error rate: " + error + "%");
    }

    public static List<List<String>> readData(String url) {
        final String COMMA_DELIMITER = ",";
        List<List<String>> records = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(url))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;
    }

    public static List<String> bayesian_classification(List<List<String>> data_train, List<List<String>> data_test) {
        List<String> labels = get_labels(data_train);
        // buat List<String> penampung result untuk tiap baris data_test
        List<String> result = new ArrayList<>();
        // perulangan sepanjang data_test
        for (int i = 0; i < data_test.size(); i++) {
            // buat List<String> penampung probabilitas untuk tiap data test terhadap label
            List<Double> probabilities = new ArrayList<>();
            // perulangan sepanjang labels
            for (int j = 0; j < labels.size(); j++) {
                // cari probabilitas dan return hasilnya ke sebuah var
                double probability = get_probability(data_train, labels.get(j), data_test.get(i));
                // assign hasil probabilitas ke list penampung probabilitas
                probabilities.add(probability);
            }
            // cari probabilitas terbesar
            int resultIndex = get_biggest_probability_index(probabilities);
            // assign ke penampung result
            result.add(
                    probabilities.get(resultIndex) != 0 ? labels.get(resultIndex) : "No result [any new param value]");
        }
        // return penampung result
        return result;
    }

    public static List<String> get_labels(List<List<String>> data_train) {
        List<String> labels = new ArrayList<>();
        int label_index = data_train.get(0).size() - 1;

        for (List<String> line : data_train) {
            String trainLabel = line.get(label_index);

            if (labels.isEmpty()) {
                labels.add(trainLabel);
            } else {
                boolean labelExist = false;

                for (String label : labels) {
                    if (label.equals(trainLabel)) {
                        labelExist = true;
                        break;
                    }
                }

                if (!labelExist) {
                    labels.add(trainLabel);
                }
            }
        }

        return labels;
    }

    public static double get_probability(List<List<String>> data_train, String label, List<String> params) {
        if (params == null) { // nyari persentase banyaknya label per keseluruhan data
            int count = 0;
            int label_index = data_train.get(0).size() - 1;
            for (List<String> line : data_train) {
                if (line.get(label_index).equals(label)) {
                    count++;
                }
            }

            return (double) count / (double) data_train.size();
        } else { // nyari persentase banyaknya data dengan param tertentu terhadap sebuah label
                 // per banyaknya label
            double label_probability = get_probability(data_train, label, null);
            int label_index = data_train.get(0).size() - 1;

            double param_probability = 1;
            for (int i = 0; i < params.size() - 1; i++) {
                int param_found = 0;
                int label_found = 0;

                for (List<String> train_row : data_train) {
                    if (train_row.get(label_index).equals(label)) {
                        if (train_row.get(i).equals(params.get(i)) && train_row.get(label_index).equals(label)) {
                            param_found++;
                        }
                        label_found++;
                    }
                }

                param_probability *= ((double) param_found / (double) label_found);
            }

            return param_probability * label_probability;
        }
    }

    public static int get_biggest_probability_index(List<Double> probabilities) {
        int index = 0;

        for (int i = 0; i < probabilities.size(); i++) {
            if (probabilities.get(index) < probabilities.get(i)) {
                index = i;
            }
        }

        return index;
    }

    public static double results_error(List<List<String>> data_test, List<String> results, boolean percentage_return) {
        double error = 0;
        int data_test_label_index = data_test.get(0).size() - 1;

        for (int i = 0; i < data_test.size(); i++) {
            if (!data_test.get(i).get(data_test_label_index).equals(results.get(i))) {
                error++;
            }
        }

        return percentage_return ? error * 100 / results.size() : error / results.size();
    }
}